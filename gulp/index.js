'use strict';

var fs                  = require('fs'),
    gulp                = require('gulp'),
    gutil               = require('gulp-util'),
    livereload          = require('gulp-livereload');

fs.readdirSync(__dirname + '/tasks').forEach(function(file){
  require(__dirname + '/tasks/' + file);
});

//var defaultTasks = ['imagemin', 'scripts', 'styles'];

var defaultTasks = ['imagemin', 'styles'];

if (!gutil.env.production){
  livereload.listen();
  defaultTasks.push('watch');
} else {
  defaultTasks.push('generate-favicon');
}

gulp.task('default', defaultTasks);

