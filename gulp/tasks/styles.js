'use strict';

let gulp              = require('gulp'),
  gutil               = require('gulp-util'),
  livereload          = require('gulp-livereload'),
  notify              = require('gulp-notify'),
  postcss             = require('gulp-postcss'),
  rename              = require('gulp-rename'),
  sass                = require('gulp-sass'),
  sourcemaps          = require('gulp-sourcemaps'),
  cssvariables        = require('postcss-css-variables');

gulp.task('styles', function() {
  return gulp.src(['src/scss/screen.scss', 'src/scss/ie.scss'])
    .pipe(sourcemaps.init())
    .pipe(sass())
    .on('error', notify.onError({
      icon:       'gulp/developer/icon-notify-styles.jpg',
      title:      'SCSS error',
      message:    '<%= error.message %>'
    }))
    .pipe(postcss([
      require('autoprefixer')({ browsers: [ '> 2% in NL' ]})
    ]))
    .pipe(gutil.env.production ? require('gulp-cssnano')() : gutil.noop())
    .pipe(sourcemaps.write('.'))
    .pipe(gutil.env.production ? rename(function(path) {
      path.basename = path.extname === '.map' ?
        path.basename.replace(/\.(.*)$/, '.min.$1') :
      path.basename + '.min';
    }) : gutil.noop())
    .pipe(!gutil.env.production ? livereload() : gutil.noop())
    .pipe(gulp.dest('public/dist/css'))
});
