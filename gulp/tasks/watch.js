'use strict';

let gulp              = require('gulp'),
  livereload          = require('gulp-livereload');

gulp.task('watch', function() {
  livereload.listen();
  gulp.watch([
    'src/scss/*.scss',
    'src/scss/**/*.scss',
    'src/**/*{png,jpg,gif,svg}'
  ], [ 'imagemin', 'styles' ]);
});
//  ], [ 'imagemin', 'scripts', 'styles' ]);
