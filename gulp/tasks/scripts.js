'use strict';

let browserify        = require('browserify'),
  buffer              = require('vinyl-buffer'),
  gulp                = require('gulp'),
  gutil               = require('gulp-util'),
  livereload          = require('gulp-livereload'),
  notify              = require('gulp-notify'),
  rename              = require('gulp-rename'),
  source              = require('vinyl-source-stream'),
  sourcemaps          = require('gulp-sourcemaps'),
  uglify              = require('gulp-uglify'),
  watchify            = require('watchify');

gulp.task('scripts', function(){
  let bundler = browserify({
    debug: true,
    entries: ['']
  });

  let bundle = function(){
    return bundler.bundle()
      .on('error', notify.onError({
        icon:       'gulp/developer/icon-notify-scripts.jpg',
        title:      'JavaScript error',
        message:    '<%= error.message %>'
      }))
      .pipe(source('index.js'))
      .pipe(buffer())
      .pipe(sourcemaps.init({ loadMaps: true }))
      .pipe(gutil.env.production ? uglify().on('error', function(e){
        console.log(e);
     }) : gutil.noop())
      .pipe(sourcemaps.write('.'))
      .pipe(gutil.env.production ? rename(function(path) {
        path.basename = path.extname === '.map' ?
          path.basename.replace(/\.(.*)$/, '.min.$1') :
        path.basename + '.min';
      }) : gutil.noop())
      .pipe(!gutil.env.production ? livereload() : gutil.noop())
      .pipe(gulp.dest(''));
  };
  if (!gutil.env.production){
    bundler = watchify(bundler);
    bundler.on('update', bundle);
  }
  bundler.on('log', gutil.log);
  return bundle();
});
