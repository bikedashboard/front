'use strict';

let gulp              = require('gulp'),
  imagemin            = require('gulp-imagemin'),
  merge               = require('merge-stream'),
  pngquant            = require('imagemin-pngquant');

gulp.task('imagemin', function() {
  let inputs = ['src/img/*.+(jpg|gif|png)', 'src/img/**/*.+(jpg|gif|png)', 'src/img/**/**/*.+(jpg|gif|png)', 'src/img/*', 'src/img/**/*', 'src/img/**/**/*'],
    outputs = [ 'public/dist/img/', 'public/dist/img/', 'public/dist/img/', 'public/dist/img/', 'public/dist/img/', 'public/dist/img/' ],
    streams = [],
    stream;
  for (var i = 0; i < inputs.length; i++){
    stream = gulp.src(inputs[i])
      .pipe(imagemin({
        progressive: true,
        svgoPlugins: [{ removeViewBox: false }],
        use: [ pngquant() ]
      }))
      .pipe(gulp.dest(outputs[i]));
    streams.push(stream);
  }
  return merge.apply(merge, streams);
});
