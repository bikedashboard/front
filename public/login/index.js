$(document).ready(function () {
  var prefix = '/';
  if (window.location.href.indexOf('front') > -1) {
    prefix = '/front/';
  }
    $('#login_form').on('submit', function(e) {
        e.preventDefault();
        username = $("#email").val();
        password = $("#password").val();
        login(username, password).then(logged_in => {
            if (logged_in) {
                window.location.href = prefix+'map/overview';
            } else {
                alert("Login mislukt, probeer het opnieuw");
            }
        }

        );
    });
});