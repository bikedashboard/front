
var area = '',
  zone = '';
  
var municipality = null;
var activeGeoZones = {};
var initalized = false;



$(document).ready(function () {
  // Init with current time.
  settings.timestamp = getISO8601(new Date());
    
  var start = moment().subtract(30, 'days');
  var end = moment().subtract(1, 'days');

  function cb(start, end) {
      $('#reportrange span').html(start.format('DD-MM-YYYY') + ' - ' + end.format('DD-MM-YYYY'));
      $('#reportrange_raw_data span').html(start.format('DD-MM-YYYY') + ' - ' + end.format('DD-MM-YYYY'));
  }

  $('#reportrange').daterangepicker({
      "locale": {
        "format": "DD-MM-YYYY",
        "separator": " - ",
        "applyLabel": "Pas toe",
        "cancelLabel": "Anuleer",
        "fromLabel": "Van",
        "toLabel": "Tot en met",
        "customRangeLabel": "Handmatig",
        "weekLabel": "W",
        "daysOfWeek": [
            "Zo",
            "Ma",
            "Di",
            "Wo",
            "Do",
            "Vr",
            "Za"
        ],
        "monthNames": [
            "januari",
            "febuari",
            "maart",
            "april",
            "mei",
            "juni",
            "juli",
            "augustus",
            "september",
            "oktober",
            "november",
            "december"
        ],
        "firstDay": 1
    },
      startDate: start,
      endDate: end,
      minDate: new Date(2020, 09, 01),
      maxDate: moment().subtract(1, 'days'),
      ranges: {
          'Laatste 7 dagen': [moment().subtract(7, 'days'), moment().subtract(1, 'days')],
          'Laatste 30 dagen': [moment().subtract(30, 'days'), moment().subtract(1, 'days')],
          'Vorige maand': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      }
  }, cb);

  $('#reportrange_raw_data').daterangepicker({
    "locale": {
      "format": "DD-MM-YYYY",
      "separator": " - ",
      "applyLabel": "Pas toe",
      "cancelLabel": "Anuleer",
      "fromLabel": "Van",
      "toLabel": "Tot en met",
      "customRangeLabel": "Handmatig",
      "weekLabel": "W",
      "daysOfWeek": [
          "Zo",
          "Ma",
          "Di",
          "Wo",
          "Do",
          "Vr",
          "Za"
      ],
      "monthNames": [
          "januari",
          "febuari",
          "maart",
          "april",
          "mei",
          "juni",
          "juli",
          "augustus",
          "september",
          "oktober",
          "november",
          "december"
      ],
      "firstDay": 1
  },
    startDate: start,
    endDate: end,
    minDate: new Date(2020, 09, 01),
    maxDate: moment().subtract(1, 'days'),
    ranges: {
        'Laatste 7 dagen': [moment().subtract(7, 'days'), moment().subtract(1, 'days')],
        'Laatste 90 dagen': [moment().subtract(90, 'days'), moment().subtract(1, 'days')],
        'Laatste 180 dagen': [moment().subtract(180, 'days'), moment().subtract(1, 'days')],
        'Laatste jaar': [moment().subtract(365, 'days'), moment().subtract(1, 'days')],
        'Vorige maand': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    }
}, cb);

  cb(start, end);
  active_operators = [];
  loadFilterValuesForUserValues().then(res => {
    var options = document.getElementById("municipality-selection");
    res.municipalities.forEach(municipality => {
      var option = document.createElement("option");
      option.text = municipality.name;
      option.value = municipality.gm_code;
      options.add(option); 
    });

    if (res.filter_operator == true) { 
      res.operators.forEach(operator => {
        active_operators.push(operator.system_id);
      });
    }

    if (!(res.filter_municipality && res.is_contact_person_municipality == false && res.is_admin == false)) {
      document.getElementById("download-menu-raw-data").style.display = "block";
    }
  });
  
  var download_button = document.getElementById("download_button");
  download_button.addEventListener("click", download_report);

  var download_button2 = document.getElementById("download_button_raw_data");
  download_button2.addEventListener("click", download_raw_data);

  
});

function download_report() {
  var gm_code = document.getElementById("municipality-selection").value;
  var start_date = $('#reportrange').data('daterangepicker').startDate.format("YYYY-MM-DD");
  var end_date = $('#reportrange').data('daterangepicker').endDate.format("YYYY-MM-DD");
  downloadReport(start_date, end_date, gm_code, active_operators);
}

function download_raw_data() {
  var start_date = $('#reportrange_raw_data').data('daterangepicker').startDate.format("YYYY-MM-DD");
  var end_date = $('#reportrange_raw_data').data('daterangepicker').endDate.format("YYYY-MM-DD");
  downloadRawData(start_date, end_date);
}



