var area = '',
  zone = '';
  
var municipality = null;
var activeGeoZones = {};
var initalized = false;
var ctx = document.getElementById('canvas').getContext('2d');
var ctx2 = document.getElementById('canvas2').getContext('2d');
var availableBikesGraph;
var availableBikesGraph2;


$(document).ready(function () {
  // Init with current time.
  settings.timestamp = getISO8601(new Date());
  loadFilterValuesForUser().then(res => {
    
    // Select all operators
    $('#filter-area input').change(function () {
      var selectedValue = $(this).val();
      var selectedLabel = $(this).siblings('label').html();
      if (selectedLabel) {
        $('.filter-area .filter-selected').html('<span>' + selectedLabel + '</span>');
      } else {
        $('.filter-area .filter-selected').html('<span>Selecteer een gebied</span>');
      }
      if (initalized) { reset(); } else { initalized = true; }

      getZonesMunicipality(selectedValue).then(value => addZones(value.zones));

    });


    $('#filter-operator input').change(function () {
      var selectedValue = $(this).val();
      var selectedLabel = $(this).siblings('label').html();

      if ($(this).prop("checked")) {
        settings.operators.add(selectedValue);
        selectedOperators[selectedValue] = selectedLabel;
      } else {
        settings.operators.delete(selectedValue);
        delete selectedOperators[selectedValue];
      }
      $('.filter-operator').find('.filter-selected').html('');
      var newSelected = '';
      for (var property in selectedOperators) {
        if (selectedOperators.hasOwnProperty(property)) {
          newSelected += '<span>' + selectedOperators[property] + '</span>';
        }
      }
      if (newSelected == '') {
        newSelected = '<span>Selecteer een aanbieder</span>';
      }
      $('.filter-operator').find('.filter-selected').html(newSelected);

      update();
    });
    update();
  });

  var now = new Date();
  // yesterday
  now.setDate(now.getDate() - 14);

  var configStart = {
    target: 'date_start',
    disableAmPm: true,
    timestamp: now.getTime(),
    hours: {
      min: 0,
      max: 23,
      step: 1
    },
    minutes: {
      min: 0,
      max: 59,
      step: 15
    }
  };

  var configEnd = {
    target: 'date_end',
    disableAmPm: true,
    hours: {
      min: 0,
      max: 23,
      step: 1
    },
    minutes: {
      min: 0,
      max: 59,
      step: 15
    }
  };
  var datePickerStart = new MtrDatepicker(configStart);
  var datePickerEnd = new MtrDatepicker(configEnd);

  //settings.timestampStart = datePickerStart.format('YYYY-MM-DDTHH:mm') + ':00Z';
  settings.timestampStart =  mtrToISO8601(datePickerStart);
  settings.timestampEnd = mtrToISO8601(datePickerEnd);

  $('.date').find('.filter-selected').html('<span>' + datePickerStart.format('DD-MM-YYYY HH:mm') + ' - ' + datePickerEnd.format('DD-MM-YYYY HH:mm') + '</span>');

  datePickerStart.onChange('date', function () {
    settings.timestampStart = mtrToISO8601(datePickerStart);
    $('.date').find('.filter-selected').html('<span>' + datePickerStart.format('DD-MM-YYYY HH:mm') + ' - ' + datePickerEnd.format('DD-MM-YYYY HH:mm') + '</span>');
    update();
  });

  datePickerStart.onChange('time', function (res, res2, res3) {
    // Add custom function to update hours when changing minutes
    if (res2 > res3 || (res3 == 45 && res2 == 0)) {
      // arrow up
      if (res2 == 0) {
        if (parseInt(datePickerStart.format('h')) == 23) {
          datePickerStart.setHours(0)
        } else {
          datePickerStart.setHours(parseInt(datePickerStart.format('h')) + 1)
        }
      }
    } else {
      // arrow down
      if (res2 == 0) {
        if (parseInt(datePickerStart.format('h')) == 0) {
          datePickerStart.setHours(23)
        } else {
          datePickerStart.setHours(parseInt(datePickerStart.format('h')) - 1)
        }

      }
    }
    $('.date').find('.filter-selected').html('<span>' + datePickerStart.format('DD-MM-YYYY HH:mm') + ' - ' + datePickerEnd.format('DD-MM-YYYY HH:mm') + '</span>');
    settings.timestampStart = mtrToISO8601(datePickerStart);
    update();
  });

  datePickerEnd.onChange('date', function () {
    settings.timestampEnd = mtrToISO8601(datePickerEnd);
    $('.date').find('.filter-selected').html('<span>' + datePickerStart.format('DD-MM-YYYY HH:mm') + ' - ' + datePickerEnd.format('DD-MM-YYYY HH:mm') + '</span>');
    update();
  });

  datePickerEnd.onChange('time', function (res, res2, res3) {
    // Add custom function to update hours when changing minutes
    if (res2 > res3 || (res3 == 45 && res2 == 0)) {
      // arrow up
      if (res2 == 0) {
        if (parseInt(datePickerEnd.format('h')) == 23) {
          datePickerEnd.setHours(0)
        } else {
          datePickerEnd.setHours(parseInt(datePickerEnd.format('h')) + 1)
        }
      }
    } else {
      // arrow down
      if (res2 == 0) {
        if (parseInt(datePickerEnd.format('h')) == 0) {
          datePickerEnd.setHours(23)
        } else {
          datePickerEnd.setHours(parseInt(datePickerEnd.format('h')) - 1)
        }

      }
    }

    settings.timestampEnd = mtrToISO8601(datePickerEnd);
    $('.date').find('.filter-selected').html('<span>' + datePickerStart.format('DD-MM-YYYY HH:mm') + ' - ' + datePickerEnd.format('DD-MM-YYYY HH:mm') + '</span>');

    update();
  });

  $('.filter-name').on('click', function (e) {
    e.preventDefault();
    $('.filter').removeClass('visible');
    $(this).parent().addClass('visible');
  });

  $(document).mouseup(function (e) {
    var container = $(".filter");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0) {
      container.removeClass('visible');
    }
  });
});

// Add zones to menu 
function addZones(zones) {
  $('#filter-zone').empty();
  zones.forEach(function (element) {
    if (element.zone_type == 'municipality') {
      showZone(element.zone_id, element.name);
      municipality = element;
    } else if (element.zone_type != 'neighborhood') {
      var $html = ''
      $html += '<div class="checkbox">'
      $html += '<input type="checkbox" value="' + element.zone_id + '" id="' + element.zone_id + '" name="' + element.zone_id + '"/>'
      $html += '<label for="' + element.zone_id + '">' + element.name + '</label>'
      $html += '</div>'
      $('#filter-zone').append($html)
    }
  })

  $('#filter-zone input').change(function () {
    var selectedValue = $(this).val();
    var selectedLabel = $(this).siblings('label').html();
    showZone(selectedValue, selectedLabel);
  });
}

var zoneFilterActiveZones = {};

// Show a specific zone on map, this function is toggled by checkboxes.
function showZone(zone_id, zone_label) {
  if (zone_id in zoneFilterActiveZones ) {
    settings.active_zones.delete(zone_id);
    delete zoneFilterActiveZones[zone_id];
    if (municipality && Object.keys(zoneFilterActiveZones).length == 0 ) {
      settings.active_zones.add(municipality.zone_id);
      zoneFilterActiveZones[municipality.zone_id] = municipality.name;
    }
  } else {
    if (municipality && zone_id != municipality.zone_id) {
      delete zoneFilterActiveZones[municipality.zone_id];
      settings.active_zones.delete(municipality.zone_id);
    }
    zoneFilterActiveZones[zone_id] = zone_label;
    settings.active_zones.add(zone_id);
  }
  refreshZoneInfo();
  update();
}

function refreshZoneInfo() {
  $filterZone = $('.filter-zone');
  $filterZone.find('.filter-selected').html('');
  var newSelected = '';
  for (var property in zoneFilterActiveZones) {
    if (zoneFilterActiveZones.hasOwnProperty(property)) {
      newSelected += '<span>' + zoneFilterActiveZones[property] + '</span>';
    }
  }
  if (newSelected == '') {
    newSelected = '<span>Alle zones</span>';
  }
  $filterZone.find('.filter-selected').html(newSelected);
}


function update() {
  plotHistory();
}

// Plot trips on map.
function plotHistory() {
  getHistory().then(json => {
    plotHistoryGraphs(json);
  });
  //getParkEvents(settings.timestamp).then(value => {
  //  
  //  plotParkEvents(value, date_o)
  //});
}

function getSum(total, num) {
  return total + num;
}

function getTimeColor(index) {
  var colors = ['#4cbb17', '#fce205', '#ef7215', '#311432', '#0080fe', '#ff0800'];
  return colors[index];
}

function plotHistoryGraphs(history) {
  // Clear table
  if (availableBikesGraph) {
    availableBikesGraph.destroy();
  }
  if (availableBikesGraph2) {
    availableBikesGraph2.destroy();
  }

  console.log(history)
  $("#park_event_table tbody tr").remove();
  var lines = [];

  var labels = history[0].values.available_bikes.timestamp.map(timestamp => {
    var newDate = new Date(timestamp);
    return newDate.getDate() + '-' + (newDate.getMonth() + 1) + '-' + newDate.getFullYear();
  });

  var bars = [];
  var total = [];

  history.forEach(operator => {
    console.log(operator.values.available_bikes);
    operator.values.available_bikes.stats.forEach((row, i) => {
      if (total[i]) {
        console.log(total[i]);
        total[i].forEach((value, index) => {
          if (row[index]) {
            total[i][index] = total[i][index] + row[index];
            console.log(i)
            console.log(index);
          }
        });
      } else {
        total[i] = JSON.parse(JSON.stringify(row));
      }
    });
  });

  history.forEach(operator => {
    var line = [];
    operator.values.available_bikes.stats.forEach(row => {
      line.push(row.reduce(getSum));
    });
    lines.push({
      label: operator.name,
      backgroundColor: getOperatorColor(operator.name),
      borderColor: getOperatorColor(operator.name),
      data: line,
      fill: false
    });
  });

  for (index = 0; index < 6; index++) {
    data_item = [];
    total.forEach((item) => {
        data_item.push(item[index]);
    });
    bars.push({
      label: 'totaal' + index,
      stack: 'totaal',
      backgroundColor: getTimeColor(index),
      borderColor: getTimeColor(index),
      data: data_item,
      fill: false
    });
    console.log(bars);
  }

  var config = {
    type: 'line',
    data: {
      labels: labels,
      datasets: lines
    },
    options: {
      responsive: true,
      tooltips: {
        mode: 'index',
        intersect: false,
      },
      hover: {
        mode: 'nearest',
        intersect: true
      },
      scales: {
        xAxes: [{
          display: true,
          scaleLabel: {
            display: true,
            labelString: 'Datum'
          }
        }],
        yAxes: [{
          display: true,
          scaleLabel: {
            display: true,
            labelString: 'Aantal fietsen'
          }
        }]
      }
    }
  };

  var config2 = {
    type: 'bar',
    data: {
      labels: labels,
      datasets: bars
    },
    options: {
      legend: {
        display: false
      },
      responsive: true,
      tooltips: {
        enabled: false,
        mode: 'index',
        intersect: false,
      },
      hover: {
        mode: 'nearest',
        intersect: true
      },
      scales: {
        xAxes: [{
          display: true,
          stacked: true,
          scaleLabel: {
            display: true,
            labelString: 'Datum'
          }
        }],
        yAxes: [{
          display: true,
          stacked: true,
          scaleLabel: {
            display: true,
            labelString: 'Aantal fietsen'
          }
        }]
      }
    }
  };

  availableBikesGraph = new Chart(ctx, config);
  availableBikesGraph2 = new Chart(ctx2, config2);

}

function add(accumulator, a) {
  return accumulator + a;
}

function reset() {
  zoneFilterActiveZones = {};
  for (zone_id in activeGeoZones) {
    //deleteLayer(activeGeoZones[zone_id]);
    delete activeGeoZones[zone_id];
    settings.active_zones.delete(zone_id);
  }
  reset_zone_filter();
  refreshZoneInfo();
  update();
}


