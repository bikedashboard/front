var active_layer = null;

var map = L.map('map', {
  zoomDelta: 0.25,
  zoomSnap: 0}).setView([52.0, 4.7], 9);

L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/streets-v11/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoic3ZlbjRhbGwiLCJhIjoiY2pndHI1Zjk5MTRleTJxczM1cXp0dDFmYyJ9.q2gfHAYo0g84ltS2pzWJ-Q', {
  attribution: '<a href="https://www.mapbox.com/about/maps/">Mapbox</a>; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>;<strong><a href="https://gitlab.com/bikedashboard/dashboard/wikis/home">Deelfiets Dashboard</a></strong> ',
  maxZoom: 18
}).addTo(map);
var active_edit_zone = 0;

//L.control.locate().addTo(map);
var currentLayer;

var drawnItems = new L.FeatureGroup();
map.addLayer(drawnItems);
var editZones = {};

var options = {
  position: 'topleft',
  draw: {
      polyline: false,
      polygon: false,
      circle: false, // Turns off this drawing tool
      rectangle: false,
      marker: false,
      circlemarker: false
  },
  edit: {
    featureGroup: drawnItems, //REQUIRED!!
    remove: false,
    edit: false
  }
};
var drawControl = new L.Control.Draw(options);
map.addControl(drawControl);


map.on(L.Draw.Event.CREATED, function (e) {
  var type = e.layerType;
  var layer = e.layer;

  currentLayer = layer;

  drawnItems.addLayer(layer);
});

map.on('draw:drawstart', function (e) {
  if (currentLayer != null) {
    drawnItems.removeLayer(currentLayer);
    currentLayer = null;
  }
});



$(document).ready(function () {

  loadFilterValuesForUserValues().then(result => {
      result.municipalities.forEach(item => {
        
        $('#municipality').append('<option value="' + item.gm_code + '">' + item.name + '</option>');
      });
      if (result.municipalities[0].gm_code != null) {
        loadMunicipality(result.municipalities[0].gm_code);
      }
  });

  $('#municipality').change(function () {
    var selectedValue = $(this).val();
    loadMunicipality(selectedValue)

   
  });
  

  $('#show_all').click(function () {
    showAllCustomLayers();
  });

  $('#save').click(function () {
    saveShape();
  });

  $('#new').click(function () {
    resetMenu();
    showAllCustomLayers();
    polygon = new L.Draw.Polygon(map, {
      allowIntersection: false, // Restricts shapes to simple polygons
      drawError: {
          color: '#e1e100', // Color the shape will turn when intersects
          message: 'Polygonen kunnen zichzelf niet doorsnijden!' // Message that will show when intersect
      },
      shapeOptions: {
          "stroke": true,
          "color": "#7F7FFF",
          "weight": 2,
          "stroke-width": 5,
          "dashArray": "1 1 2",
          "opacity": 0.1
      }
    });
    polygon.enable();
    $('#new_zone').show();
  });

  $('#edit_complete').click(function () {
    
    var name = $(active_edit_zone).attr('zone_name');
    console.log(name);
    saveZone($('#municipality').val(), name, currentLayer.toGeoJSON()).then(result => {
      if (result.status == 400) {
        $('#result').text("Opslaan mislukt, gebied wat je probeert te tekenen ligt buiten de gemeente grens.");
        return;
      }
      result.json().then(item => {
        $('#result').text("Zone bewerkt.");
        $('#custom_zones').append("<li class='zone' zone_id='"+ item.zone_id  + "' zone_name='" + item.name + "'>" + item.name + "<span class='close'>x</span></li> ");
        $( ".close" ).on( "click", function(event) {
          event.stopPropagation(); 
          result = deleteZone($(this).parent());
        }); 
        initListener();
        editZones[item.zone_id] = currentLayer;
        deleteZone(active_edit_zone);
        resetMenu();
        //showAllCustomLayers();
      })
    });

  });


  resetMenu();
  

});



function loadMunicipality(municipality_code) {
  if (active_layer != null) {
    deleteLayer(active_layer);
  }

  $('#custom_zones').empty();
 
  getZonesMunicipality(municipality_code)
    .then(result => {
      result.zones.forEach(item => {
        resetMenu();
        if (item.zone_type == 'municipality') {
          var style = {
            "stroke": true,
            "color": "#0072c2",
            "weight": 2,
            "stroke-width": 5,
            "dashArray": "4 1 2",
            "fill": false
          };
          getZones(item.zone_id).then(result => {
            active_layer = plotZone(result.zones[0], style);
            console.log(result.zones[0]);
            map.fitBounds(active_layer.getBounds());
          });
        } else if (item.zone_type == 'custom') {
          $('#custom_zones').append("<li class='zone' zone_id='"+ item.zone_id  + "' zone_name='" + item.name + "'>" + item.name + "<span class='close'>x</span></li> ");
          getZones(item.zone_id).then(result => {
            console.log(result);
            var style = {
              "stroke": true,
              "color": "#7F7FFF",
              "weight": 2,
              "stroke-width": 5,
              "dashArray": "4 2 2",
              "fill": true
            };
            data = L.geoJSON(result.zones[0]["geojson"], { style: style });
            data.eachLayer(function(layer) {
                editZones[item.zone_id] = layer;
                drawnItems.addLayer(editZones[item.zone_id])
            });
          });
          //console.log(item.geojson);
          
          
          //map.addLayer(drawnItems);
          //console.log(data);

          
          
        }
      });
      $( ".close" ).on( "click", function(event) {
        event.stopPropagation(); 
        result = deleteZone($(this).parent());
      });  
      
      initListener();
    });
    
}

function hideAllCustomLayers() {
  for (var key in editZones) {
    editZones[key].editing.disable()
    drawnItems.removeLayer(editZones[key]);
  }
}

function showAllCustomLayers() {
  resetMenu();
  for (var key in editZones) {
    editZones[key].editing.disable()
    drawnItems.addLayer(editZones[key]);
  }
}


function saveShape() {
  console.log();
  if ($('#name').val() == "") {
    $('#result').text("Opslaan mislukt, vul naam in");
    return;
  }
  if (currentLayer == null) {
    $('#result').text("Opslaan mislukt, geen layer getekend");
    return;
  }
  saveZone($('#municipality').val(), $('#name').val(), currentLayer.toGeoJSON()).then(result => {
    if (result.status == 400) {
      $('#result').text("Opslaan mislukt, gebied wat je probeert te tekenen ligt buiten de gemeente grens.");
      return;
    }
    result.json().then(item => {
      $('#result').text("Layer opgeslagen, zone_id is: " + item.zone_id);
      $('#custom_zones').append("<li class='zone' zone_id='"+ item.zone_id  + "' zone_name='" + item.name + "'>" + item.name + "<span class='close'>x</span></li> ");
      $( ".close" ).on( "click", function(event) {
        event.stopPropagation(); 
        result = deleteZone($(this).parent());
      }); 
      initListener();
      editZones[item.zone_id] = currentLayer;
      resetMenu();
    })
  }); 
}


function deleteZone(zone) {
  zone_id = $( zone ).attr("zone_id");
  if (zone_id == undefined) {
    console.log("Zone bestaat niet");
    return;
  }
  deleteZoneServer(zone_id).then(result => {
    if (result.deleted == true) {
      $( zone ).remove();
      drawnItems.removeLayer(editZones[zone_id]);
      delete editZones[zone_id];
      resetMenu();
      showAllCustomLayers();
    }
  });

}

function initListener() {
  $( ".zone" ).on( "click", function() {
    hideAllCustomLayers();

    zone_id = $(this).attr("zone_id");
   
    drawnItems.addLayer(editZones[zone_id]);
    console.log(editZones[zone_id]);
    editZones[zone_id].editing.enable();
    $('#edit').show();
    active_edit_zone = this;
    currentLayer = editZones[zone_id];

  });
}

function resetMenu() {
  $('#edit').hide();
  $('#new_zone').hide();

}


