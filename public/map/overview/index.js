var map = L.map('map', {
  zoomDelta: 0.25,
  zoomSnap: 0
}).setView([52.0, 4.7], 9);
L.control.locate().addTo(map);


L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/streets-v11/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoic3ZlbjRhbGwiLCJhIjoiY2pndHI1Zjk5MTRleTJxczM1cXp0dDFmYyJ9.q2gfHAYo0g84ltS2pzWJ-Q', {
  attribution: '<a href="https://www.mapbox.com/about/maps/">Mapbox</a>; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>;<strong><a href="https://gitlab.com/bikedashboard/dashboard/wikis/home">Deelfiets Dashboard</a></strong> ',
  maxZoom: 18
}).addTo(map);

//L.control.locate().addTo(map);

var area = '',
  zone = '';

var activeGeoZones = {};


$(document).ready(function () {
  // Init with current time.
  settings.timestamp = getISO8601(new Date());
  update();
  loadFilterValuesForUser().then(res => {

    // Select all operators
    $('#filter-area input').change(function () {
      var selectedValue = $(this).val();
      var selectedLabel = $(this).siblings('label').html();
      if (selectedLabel) {
        $('.filter-area .filter-selected').html('<span>' + selectedLabel + '</span>');
      } else {
        $('.filter-area .filter-selected').html('<span>Selecteer een gebied</span>');
      }
      reset();
      getZonesMunicipality(selectedValue).then(value => addZones(value.zones));
      
    });

    
    $('#filter-operator input').change(function () {
      var selectedValue = $(this).val();
      var selectedLabel = $(this).siblings('label').html();
  
      if ($(this).prop("checked")) {
        settings.operators.add(selectedValue);
        selectedOperators[selectedValue] = selectedLabel;
      } else {
        settings.operators.delete(selectedValue);
        delete selectedOperators[selectedValue];
      }
      $('.filter-operator').find('.filter-selected').html('');
      var newSelected = '';
      for (var property in selectedOperators) {
        if (selectedOperators.hasOwnProperty(property)) {
          newSelected += '<span>' + selectedOperators[property] + '</span>';
        }
      }
      if (newSelected == '') {
        newSelected = '<span>Selecteer een aanbieder</span>';
      }
      $('.filter-operator').find('.filter-selected').html(newSelected);
  
      update();
  });
  });

  var config = {
    target: 'date',
    disableAmPm: true,
    hours: {
      min: 0,
      max: 23,
      step: 1
    },
    minutes: {
      min: 0,
      max: 59,
      step: 15
    }
  };
  var datePicker = new MtrDatepicker(config);
  $('.date').find('.filter-selected').html('<span>'+datePicker.format('DD-MM-YYYY HH:mm')+'</span>');

  datePicker.onChange('date', function () {
    settings.timestamp = mtrToISO8601(datePicker);
    $('.date').find('.filter-selected').html('<span>'+datePicker.format('DD-MM-YYYY HH:mm')+'</span>');
    update();
  });

  datePicker.onChange('time', function (res, res2, res3) {
    // Add custom function to update hours when changing minutes
    if (res2 > res3 || (res3 == 45 && res2 == 0)) {
      // arrow up
      if (res2 == 0) {
        if (parseInt(datePicker.format('h')) == 23) {
          datePicker.setHours(0)
        } else {
          datePicker.setHours(parseInt(datePicker.format('h')) + 1)
        }
      }
    } else {
      // arrow down
      if (res2 == 0) {
        if (parseInt(datePicker.format('h')) == 0) {
          datePicker.setHours(23)
        } else {
          datePicker.setHours(parseInt(datePicker.format('h')) - 1)
        }

      }
    }

    settings.timestamp = mtrToISO8601(datePicker);
    $('.date').find('.filter-selected').html('<span>'+datePicker.format('DD-MM-YYYY HH:mm')+'</span>');

    update();
  });



  $('.filter-name').on('click', function (e) {
    e.preventDefault();
    $('.filter').removeClass('visible');
    $(this).parent().addClass('visible');
  });

  $(document).mouseup(function (e) {
    var container = $(".filter");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0) {
      container.removeClass('visible');
    }
  });
});

// Add zones to menu 
function addZones(zones) {
  $('#filter-zone').empty();
  zones.forEach(function (element) {
    if (element.zone_type == 'municipality') {
      showZone(element.zone_id, element.name);
    } else if (element.zone_type != 'neighborhood') {
      var $html = ''
      $html += '<div class="checkbox">'
      $html += '<input type="checkbox" value="' + element.zone_id + '" id="' + element.zone_id + '" name="' + element.zone_id + '"/>'
      $html += '<label for="' + element.zone_id + '">' + element.name + '</label>'
      $html += '</div>'
      $('#filter-zone').append($html)
    }
  })

  $('#filter-zone input').change(function () {
    var selectedValue = $(this).val();
    var selectedLabel = $(this).siblings('label').html();
    showZone(selectedValue, selectedLabel);
  });
}

var zoneFilterActiveZones = {};

// Show a specific zone on map, this function is toggled by checkboxes.
function showZone(zone_id, zone_label) {
  if (zone_id in activeGeoZones) {
    deleteLayer(activeGeoZones[zone_id]);
    delete activeGeoZones[zone_id];
    settings.active_zones.delete(zone_id);
    delete zoneFilterActiveZones[zone_id];
  } else {
    zoneFilterActiveZones[zone_id] = zone_label;
    getZones(zone_id).then(value => {
      activeGeoZones[zone_id] = plotZone(value["zones"][0]);
      fitBounds();
    });
    settings.active_zones.add(zone_id);
  }  
  refreshZoneInfo();
  update();
}

function refreshZoneInfo() {
  $filterZone = $('.filter-zone');
  $filterZone.find('.filter-selected').html('');
  var newSelected = '';
  for (var property in zoneFilterActiveZones) {
    if (zoneFilterActiveZones.hasOwnProperty(property)) {
      newSelected += '<span>' + zoneFilterActiveZones[property] + '</span>';
    }
  }
  if (newSelected == '') {
    newSelected = '<span>Alle zones</span>';
  }
  $filterZone.find('.filter-selected').html(newSelected);
}

function fitBounds() {
  var bounds = L.latLngBounds([]);
  for (zone in activeGeoZones) {
      layerBounds = activeGeoZones[zone].getBounds();
      bounds.extend(layerBounds);
  }
  map.fitBounds(bounds);
}

function update() {
  plotMarkers();
  showStats();
}

function plotMarkers() {
  date_o = Date.parse(settings.timestamp);
  getParkEvents(settings.timestamp).then(value => {
    clearMarkers();
    plotParkEvents(value, date_o)
  });
}

function plotStatTable(stats) {
  // Clear table
  console.log(stats)
  $("#park_event_table tbody tr").remove();
  var table = document.getElementById('park_event_table').getElementsByTagName('tbody')[0];
  stats.forEach(row => {
    console.log(row);
    addRowToTable(row.zone.name, row.number_of_bicycles_parked_for);
  })

}

function addRowToTable(name, count) {
  var table = document.getElementById('park_event_table').getElementsByTagName('tbody')[0];
  var row = table.insertRow(-1);
  var cell1 = row.insertCell(0);
  var cell2 = row.insertCell(1);
  var cell3 = row.insertCell(2);
  var cell4 = row.insertCell(3);
  var cell5 = row.insertCell(4);
  var cell6 = row.insertCell(5);
  var cell7 = row.insertCell(6);
  var cell8 = row.insertCell(7);
  cell1.innerHTML = '<span class="name-summary">'+name+'</span>';
  cell1.width = 100;
  cell2.innerHTML = count.reduce(add);
  cell3.innerHTML = count[0];
  cell4.innerHTML = count[1];
  cell5.innerHTML = count[2];
  cell6.innerHTML = count[3];
  cell7.innerHTML = count[4];
  cell8.innerHTML = count[5];
}

function add(accumulator, a) {
  return accumulator + a;
}

// Function for retreiving and showing stats.
function showStats() {
  getParkEventStats(settings.timestamp).then(result => {
    plotStatTable(result["park_event_stats"]);
  });
}

function reset() {
  zoneFilterActiveZones = {};
  for (zone_id in activeGeoZones) {
    deleteLayer(activeGeoZones[zone_id]);
    delete activeGeoZones[zone_id];
    settings.active_zones.delete(zone_id);
  }
  reset_zone_filter();
  refreshZoneInfo();
  update();
}


