
var map = L.map('map', {
  zoomDelta: 0.25,
  zoomSnap: 0
}).setView([52.0, 4.7], 9);

L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/streets-v11/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoic3ZlbjRhbGwiLCJhIjoiY2pndHI1Zjk5MTRleTJxczM1cXp0dDFmYyJ9.q2gfHAYo0g84ltS2pzWJ-Q', {
  attribution: '<a href="https://www.mapbox.com/about/maps/">Mapbox</a>; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>;<strong><a href="https://gitlab.com/bikedashboard/dashboard/wikis/home">Deelfiets Dashboard</a></strong> ',
  maxZoom: 18
}).addTo(map);
//L.control.locate().addTo(map);

var area = '',
  zone = '';

var activeGeoZones = {};


$(document).ready(function () {
  // Init with current time.
  settings.timestamp = getISO8601(new Date());
  loadFilterValuesForUser().then(res => {

    // Select all operators
    $('#filter-area input').change(function () {
      var selectedValue = $(this).val();
      var selectedLabel = $(this).siblings('label').html();
      if (selectedLabel) {
        $('.filter-area .filter-selected').html('<span>' + selectedLabel + '</span>');
      } else {
        $('.filter-area .filter-selected').html('<span>Selecteer een gebied</span>');
      }
      reset();
      getZonesMunicipality(selectedValue).then(value => addZones(value.zones));

    });


    $('#filter-operator input').change(function () {
      var selectedValue = $(this).val();
      var selectedLabel = $(this).siblings('label').html();

      if ($(this).prop("checked")) {
        settings.operators.add(selectedValue);
        selectedOperators[selectedValue] = selectedLabel;
      } else {
        settings.operators.delete(selectedValue);
        delete selectedOperators[selectedValue];
      }
      $('.filter-operator').find('.filter-selected').html('');
      var newSelected = '';
      for (var property in selectedOperators) {
        if (selectedOperators.hasOwnProperty(property)) {
          newSelected += '<span>' + selectedOperators[property] + '</span>';
        }
      }
      if (newSelected == '') {
        newSelected = '<span>Selecteer een aanbieder</span>';
      }
      $('.filter-operator').find('.filter-selected').html(newSelected);

      update();
    });
  });

  var now = new Date();
  // yesterday
  now.setDate(now.getDate() - 1);

  var configStart = {
    target: 'date_start',
    disableAmPm: true,
    timestamp: now.getTime(),
    hours: {
      min: 0,
      max: 23,
      step: 1
    },
    minutes: {
      min: 0,
      max: 59,
      step: 15
    }
  };

  var configEnd = {
    target: 'date_end',
    disableAmPm: true,
    hours: {
      min: 0,
      max: 23,
      step: 1
    },
    minutes: {
      min: 0,
      max: 59,
      step: 15
    }
  };
  var datePickerStart = new MtrDatepicker(configStart);
  var datePickerEnd = new MtrDatepicker(configEnd);

  settings.timestampStart = mtrToISO8601(datePickerStart);
  settings.timestampEnd = mtrToISO8601(datePickerEnd);

  $('.date').find('.filter-selected').html('<span>' + datePickerStart.format('DD-MM-YYYY HH:mm') + ' - ' + datePickerEnd.format('DD-MM-YYYY HH:mm') + '</span>');

  datePickerStart.onChange('date', function () {
    settings.timestampStart = mtrToISO8601(datePickerStart);
    $('.date').find('.filter-selected').html('<span>' + datePickerStart.format('DD-MM-YYYY HH:mm') + ' - ' + datePickerEnd.format('DD-MM-YYYY HH:mm') + '</span>');
    update();
  });

  datePickerStart.onChange('time', function (res, res2, res3) {
    // Add custom function to update hours when changing minutes
    if (res2 > res3 || (res3 == 45 && res2 == 0)) {
      // arrow up
      if (res2 == 0) {
        if (parseInt(datePickerStart.format('h')) == 23) {
          datePickerStart.setHours(0)
        } else {
          datePickerStart.setHours(parseInt(datePickerStart.format('h')) + 1)
        }
      }
    } else {
      // arrow down
      if (res2 == 0) {
        if (parseInt(datePickerStart.format('h')) == 0) {
          datePickerStart.setHours(23)
        } else {
          datePickerStart.setHours(parseInt(datePickerStart.format('h')) - 1)
        }

      }
    }
    $('.date').find('.filter-selected').html('<span>' + datePickerStart.format('DD-MM-YYYY HH:mm') + ' - ' + datePickerEnd.format('DD-MM-YYYY HH:mm') + '</span>');
    settings.timestampStart = mtrToISO8601(datePickerStart);
    update();
  });

  datePickerEnd.onChange('date', function () {
    settings.timestampEnd = mtrToISO8601(datePickerEnd);
    $('.date').find('.filter-selected').html('<span>' + datePickerStart.format('DD-MM-YYYY HH:mm') + ' - ' + datePickerEnd.format('DD-MM-YYYY HH:mm') + '</span>');
    update();
  });

  datePickerEnd.onChange('time', function (res, res2, res3) {
    // Add custom function to update hours when changing minutes
    if (res2 > res3 || (res3 == 45 && res2 == 0)) {
      // arrow up
      if (res2 == 0) {
        if (parseInt(datePickerEnd.format('h')) == 23) {
          datePickerEnd.setHours(0)
        } else {
          datePickerEnd.setHours(parseInt(datePickerEnd.format('h')) + 1)
        }
      }
    } else {
      // arrow down
      if (res2 == 0) {
        if (parseInt(datePickerEnd.format('h')) == 0) {
          datePickerEnd.setHours(23)
        } else {
          datePickerEnd.setHours(parseInt(datePickerEnd.format('h')) - 1)
        }

      }
    }

    settings.timestampEnd = mtrToISO8601(datePickerEnd);
    $('.date').find('.filter-selected').html('<span>' + datePickerStart.format('DD-MM-YYYY HH:mm') + ' - ' + datePickerEnd.format('DD-MM-YYYY HH:mm') + '</span>');

    update();
  });

  $('.filter-name').on('click', function (e) {
    e.preventDefault();
    $('.filter').removeClass('visible');
    $(this).parent().addClass('visible');
  });

  $(document).mouseup(function (e) {
    var container = $(".filter");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0) {
      container.removeClass('visible');
    }
  });
});

// Add zones to menu 
function addZones(zones) {
  $('#filter-zone').empty();
  zones.forEach(function (element) {
    if (element.zone_type == 'municipality') {
      showZone(element.zone_id, element.name);
    } else if (element.zone_type != 'neighborhood') {
      var $html = ''
      $html += '<div class="checkbox">'
      $html += '<input type="checkbox" value="' + element.zone_id + '" id="' + element.zone_id + '" name="' + element.zone_id + '"/>'
      $html += '<label for="' + element.zone_id + '">' + element.name + '</label>'
      $html += '</div>'
      $('#filter-zone').append($html)
    }
  })

  $('#filter-zone input').change(function () {
    var selectedValue = $(this).val();
    var selectedLabel = $(this).siblings('label').html();
    showZone(selectedValue, selectedLabel);
  });
}

var zoneFilterActiveZones = {};

// Show a specific zone on map, this function is toggled by checkboxes.
function showZone(zone_id, zone_label) {
  if (zone_id in activeGeoZones) {
    deleteLayer(activeGeoZones[zone_id]);
    delete activeGeoZones[zone_id];
    settings.active_zones.delete(zone_id);
    delete zoneFilterActiveZones[zone_id];
  } else {
    zoneFilterActiveZones[zone_id] = zone_label;
    getZones(zone_id).then(value => {
      activeGeoZones[zone_id] = plotZone(value["zones"][0]);
      fitBounds();
    });
    settings.active_zones.add(zone_id);
  }
  refreshZoneInfo();
  update();
}

function refreshZoneInfo() {
  $filterZone = $('.filter-zone');
  $filterZone.find('.filter-selected').html('');
  var newSelected = '';
  for (var property in zoneFilterActiveZones) {
    if (zoneFilterActiveZones.hasOwnProperty(property)) {
      newSelected += '<span>' + zoneFilterActiveZones[property] + '</span>';
    }
  }
  if (newSelected == '') {
    newSelected = '<span>Alle zones</span>';
  }
  $filterZone.find('.filter-selected').html(newSelected);
}

function fitBounds() {
  var bounds = L.latLngBounds([]);
  for (zone in activeGeoZones) {
    layerBounds = activeGeoZones[zone].getBounds();
    bounds.extend(layerBounds);
  }
  map.fitBounds(bounds);
  
}

function update() {
  plotTrips();
  showStats();
}

// Plot trips on map.
function plotTrips() {
  getTrips().then(json => {
    clearMarkers();
    clearLines();
    plotTripLines(json);
  });
  //getParkEvents(settings.timestamp).then(value => {
  //  
  //  plotParkEvents(value, date_o)
  //});
}



function plotStatTable(stats) {
  // Clear table
  console.log(stats)
  $("#rides_table tbody tr").remove();
  var table = document.getElementById('rides_table').getElementsByTagName('tbody')[0];
  stats.forEach(row => {
    addRowToTable(row.zone.name, row.number_of_trips);
  })

}

function addRowToTable(name, count) {
  var table = document.getElementById('rides_table').getElementsByTagName('tbody')[0];
  var row = table.insertRow(-1);
  var cell1 = row.insertCell(0);
  var cell2 = row.insertCell(1);
  var cell3 = row.insertCell(2);
  cell1.innerHTML = name;
  cell2.innerHTML = count[0];
  cell3.innerHTML = count[1];
}

function add(accumulator, a) {
  return accumulator + a;
}

// Function for retreiving and showing stats.
function showStats() {
  getTripStats().then(result => {
    plotStatTable(result["trip_stats"]);
  });
}

function reset() {
  zoneFilterActiveZones = {};
  for (zone_id in activeGeoZones) {
    deleteLayer(activeGeoZones[zone_id]);
    delete activeGeoZones[zone_id];
    settings.active_zones.delete(zone_id);
  }
  reset_zone_filter();
  refreshZoneInfo();
}


