/* Vars */
var loggedIn = false;
var userName = '';
var markers = [];
var polylines = [];
var selectedOperators = {};

var settings = {
  operators: new Set(),
  active_zones: new Set()
};

var prefix = '/';
if (window.location.href.indexOf('front') > -1) {
  prefix = '/front/';
}

function reset_zone_filter() {
  settings.active_zones.clear();
}

function getFilterUrl() {
  filter = "";
  if (settings.operators.size > 0) {
    filter += '&operators=' + Array.from(settings.operators).join(",");
  }

  if (settings.active_zones.size > 0) {
    filter += '&zone_ids=' + Array.from(settings.active_zones).join(",");
  }

  return filter;
}


/* Top bar */
document.addEventListener("DOMContentLoaded", function () {

  if (checkAuth()) {
    loggedIn = true
    userName = getCookie("authUserName")
    var accountHTML = ''
    accountHTML += '<li><a href="' + prefix + 'help">Hulp</a></li>'
    accountHTML += '<li><a href="#logout" id="logout">Uitloggen</a></li>'
    accountHTML += '<li><span class="circle">' + userName.substr(0, 1) + '</span><span class="username">' + userName + '</span></li>'
    $('ul.secondary-menu').html(accountHTML);

    $('#logout').click(function (e) {
      e.preventDefault();
      logout();
    });
  } else {
    var location = window.location.href;
    if (location.indexOf('login') == -1 && location.indexOf('password') == -1 && location.indexOf('help') == -1) {
      window.location.href = prefix + 'login';
    }
  }

  $('.toggle-menu').on('click', function (e) {
    e.preventDefault();
    $('body').removeClass('filters-visible');
    $('body').toggleClass('menu-visible');
  });

  $('.toggle-filters').on('click', function (e) {
    e.preventDefault();
    $('body').removeClass('menu-visible');
    $('body').toggleClass('filters-visible');
  });

})
/* Check auth status function */
function checkAuth() {
  return getCookie("authToken") ? true : false
}

/* Login function */
function login(username, password) {
  var url = 'https://auth.deelfietsdashboard.nl/api/login';
  var data = {
    loginId: username,
    password: password,
    applicationId: "bf901170-a2db-4f91-8ca7-24921e961193"
  };

  return fetch(url, {
    method: 'POST', // or 'PUT'
    body: JSON.stringify(data), // data can be `string` or {object}!
    headers: {
      'Content-Type': 'application/json'
    }
  }).then(res => res.json())
    .then(response => {
      setCookie("authToken", response.token, 30);
      setCookie("authUserName", response.user.username, 30);
      return true;
    }).catch(error => {
      return false;
    });

}

function resetPassword(username) {
  var url = 'https://auth.deelfietsdashboard.nl/api/user/forgot-password';
  var data = {
    loginId: username,
  };

  return fetch(url, {
    method: 'POST', // or 'PUT'
    body: JSON.stringify(data), // data can be `string` or {object}!
    headers: {
      'Content-Type': 'application/json'
    }
  }).then(res => res.json())
    .then(response => {

      return true;
    }).catch(error => {
      return false;
    });

}

function logout() {
  // Cookies are deleted by setting the time they are expiring in the past
  //var href = $(this).attr("href");
  setCookie("authToken", "", -10);
  setCookie("authUserName", "", -10);
  // Redirect to login page.
  window.location.href = prefix + 'login';
  //window.location.href = href;
}

/* Helper function for setting cookies */
function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  var expires = "expires=" + d.toGMTString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ';path=/'
  console.log(document.cookie);
}

/* Helper function for getting cookies */
function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function getHeaders() {
  return {
    headers: {
      "Authorization": "Bearer " + getCookie("authToken")
    }
  };
}

// getZones belonging to municipality
function getZonesMunicipality(gm_code) {
  url = 'https://api.deelfietsdashboard.nl/dashboard-api/zones?gm_code=' + gm_code;
  return fetch(url, getHeaders())
    .then(function (response) {
      console.log(response);
      if (response.status == 401) {
        errorNoPermission(response);
      }
      return response.json();
    });
}

// getSpecic zones based on zone_ids.
function getZones(zone_ids) {
  url = 'https://api.deelfietsdashboard.nl/dashboard-api/zones?zone_ids=' + zone_ids + '&include_geojson=true';
  return fetch(url, getHeaders())
    .then(function (response) {
      if (response.status == 401) {
        errorNoPermission(response);
      }
      return response.json();
    });
}


function getParkEvents(timestamp) {
  url = 'https://api.deelfietsdashboard.nl/dashboard-api/park_events?timestamp=' + timestamp + getFilterUrl();
  return fetch(url, getHeaders())
    .then(function (response) {
      console.log(response);
      if (response.status == 401) {
        errorNoPermission(response);
      }
      return response.json();
    });
}

function getHistory() {
  return new Promise(function (resolve, reject) {
    var result = [];
    var currentOperator;
    filter = "";
    if (settings.active_zones.size > 0) {
      filter += '&zone_ids=' + Array.from(settings.active_zones).join(",");
    }
    if (settings.operators.size > 0) {
      settings.operators.forEach(operator => {
        var newFilter = filter + '&operators=' + operator;
        currentOperator = operator;
        fetch('https://api.deelfietsdashboard.nl/dashboard-api/stats/available_bikes?start_time=' + settings.timestampStart
          + '&end_time=' + settings.timestampEnd + newFilter, getHeaders())
          .then(function (response) {
            console.log(response);
            if (response.status == 401) {
              errorNoPermission(response);
            }
            return response.json();
          })
          .then(function (json) {
            result.push({
              name: operator,
              values: json
            });
            if (result.length == settings.operators.size) {
              resolve(result);
            }
          })

      });
    }
  });

}

// plot default zone on map.
function plotZone(zone) {
  var myStyle = {
    "stroke": true,
    "fillColor": "#0072c2",
    "color": "#0072c2",
    "weight": 2,
    "stroke-width": "3",
    "opacity": 0.6,
    "fillOpacity": 0.1,
    "interactive": false
  };
  return  plotZone(zone, myStyle);
}

function plotZone(zone, style) {
  var geojsonLayer = L.geoJSON(zone["geojson"], { style: style }).addTo(map).bringToBack();
  return geojsonLayer;
}

// Delete created layer from map.
function deleteLayer(layer) {
  map.removeLayer(layer);
}

function mtrToISO8601 (mtrObject) {
  return getISO8601(new Date(mtrObject.format('YYYY-MM-DDTHH:mm')));
}

function getISO8601(timestamp) {
  return timestamp.toISOString().split(".")[0] + "Z";
}


// Error when 401 is retrieved from server.
function errorNoPermission(response) {
  alert("Log eerst in");
  window.open("login.html", "_self");
}

function readableTimestamp(dateObject) {
  return dateObject.getFullYear() + "-" + (dateObject.getMonth() + 1) + "-" + dateObject.getDate() 
    + " " + dateObject.getHours() + ":" + dateObject.getMinutes() + ":" +  dateObject.getSeconds();
}


function plotParkEvents(json, timestamp) {
  json["park_events"].forEach(function (cycle_location) {
    var difference = (new Date(timestamp) - new Date(cycle_location["start_time"])) / (60 * 60 * 1000);

    text = "<h3>Fiets " + cycle_location["bike_id"] + "</h3>";
    text += "<p><b>bike_id:</b> " + cycle_location["bike_id"] + " <br>";
    text += "<b>operator:</b> " + cycle_location["system_id"] + " <br>";
    if (difference <= 72) {
      text += "<b>Staat hier:</b> " + Math.round(difference) + " uur " +  " geparkeerd. <br>";
    } else {
      text += "<b>Staat hier:</b> " + Math.round(difference/24) + " dagen " +  " geparkeerd. <br>";
    }
    text += "<b>Geparkeerd sinds:</b> " + (new Date(cycle_location["start_time"])).toLocaleString() + "</br>";
    if (cycle_location["end_time"] != null) {
      text += "<b>Stond hier geparkeerd tot:</b> " + (new Date(cycle_location["end_time"])).toLocaleString() + "</br></p>";
    }

    var mstyle = getMarkerTimeNotUsed("uurStilstand", difference, cycle_location["system_id"]);
    // Added litle bit randomisation to show markers that are exactly on the same position.
    // Can be improved in the future to also work with trips
    // Or that only randomisation is added in case of exactly the same position.
    plotMarker(cycle_location["bike_id"], cycle_location["location"]["latitude"] + Math.random()/8000, cycle_location["location"]["longitude"] + Math.random()/8000, text, mstyle);
  });

}

function clearMarkers() {
  markers.forEach(element => {
    map.removeLayer(element);
  });
  markers = [];
}

function clearLines() {
  polylines.forEach(element => {
    map.removeLayer(element);
  });
  polylines = [];
}

function plotMarker(mbikeId, mlat, mlon, mtextdef, mstyle) {

  marker = L.circleMarker([mlat, mlon], mstyle).addTo(map);
  marker.bindPopup(mtextdef);
  markers.push(marker);
  //}
}

function getMarkerTimeNotUsed(StyleScheme, difference, operator) {
  var style_icon = {
    radius: 6,
    fillOpacity: 0.8,
    weight: 2
  };

  if (StyleScheme = "uurStilstand") {
    if (difference == null) {
      style_icon["fillColor"] = "#ccc";
    } else if (difference < 24) {
      style_icon["fillColor"] = "#4cbb17";
    } else if (difference < 2 * 24) {
      style_icon["fillColor"] = "#fce205";
    } else if (difference < 3 * 24) {
      style_icon["fillColor"] = "#ef7215";
    } else if (difference < 5 * 24) {
      style_icon["fillColor"] = "#311432";
    } else if (difference < 7 * 24) {
      style_icon["fillColor"] = "#0080fe";
    } else {
      style_icon["fillColor"] = "#ff0800";
    }
  }
  style_icon["color"] = getOperatorColor(operator);
  return style_icon;
}

function getOperatorColor(operator) {
  switch (operator) {
    case 'cykl':
      color = '#92C636';
      break;
    case 'mobike':
      color = '#b0b0b0';
      break;
    case 'flickbike':
      color = '#F43300';
      break;
    case 'donkey':
      color = "#FBAE7F";
      break;
    case 'htm':
      color = "#db291d";
      break;
    case 'jump':
      color = "#000000";
      break;
    case 'gosharing':
      color = "#70b11e";
      break;
    case 'check':
      color = "#8719ff";
      break;
    case 'felyx':
      color = "#0a3d1f";
      break;
    case 'deelfietsnederland':
      color = "#19a5f7";
      break;
    case 'baqme':
      color = "#46e0b5";
      break;
    case 'lime':
      color = "#b2f209";
      break;
    case 'keobike':
      color= "#1ba491";
      break;
    default:
      color = "#FFFFFF";
  }
  return color;

}

function getParkEventStats(timestamp) {
  url = 'https://api.deelfietsdashboard.nl/dashboard-api/park_events/stats?timestamp=' + timestamp + getFilterUrl();
  return fetch(url, getHeaders())
    .then(function (response) {
      console.log(response);
      if (response.status == 401) {
        errorNoPermission(response);
      }
      return response.json();
    });
}

function getTripStats() {
  return fetch('https://api.deelfietsdashboard.nl/dashboard-api/trips/stats?start_time=' + settings.timestampStart
    + '&end_time=' + settings.timestampEnd + getFilterUrl(), getHeaders())
    .then(function (response) {
      console.log(response);
      if (response.status == 401) {
        errorNoPermission(response);
      }
      return response.json();
    });
}

function getRentalStats() {
  return fetch('https://api.deelfietsdashboard.nl/dashboard-api/rentals/stats?start_time=' + settings.timestampStart
    + '&end_time=' + settings.timestampEnd + getFilterUrl(), getHeaders())
    .then(function (response) {
      console.log(response);
      if (response.status == 401) {
        errorNoPermission(response);
      }
      return response.json();
    });
}



function saveZone(gm_code, name, geojson) {
  url = 'https://api.deelfietsdashboard.nl/dashboard-api/zone';
  options = getHeaders();
  options.method = "PUT";
  body = {
    geojson: geojson.geometry,
    municipality: gm_code,
    name: name
  };
  options.body = JSON.stringify(body);
  return fetch(url, options)
    .then(function (response) {
      if (response.status == 401) {
        errorNoPermission(response);
      }
      return response;
    });
}

function deleteZoneServer(zone_id) {
  url = 'https://api.deelfietsdashboard.nl/dashboard-api/zone/' + zone_id; 
  options = getHeaders();
  options.method = "DELETE";
  
  return fetch(url, options)
    .then(function (response) {
      if (response.status == 401) {
        console.log("user has no permission");
      }
      return response.json();
    });

}

function loadFilterValuesForUserValues() {
  url = 'https://api.deelfietsdashboard.nl/dashboard-api/menu/acl';
  return fetch(url, getHeaders())
    .then(function (response) {
      if (response.status == 401) {
        errorNoPermission(response);
      }
      return response.json();
    });
}

function loadFilterValuesForUser() {
  url = 'https://api.deelfietsdashboard.nl/dashboard-api/menu/acl';
  return fetch(url, getHeaders())
    .then(function (response) {
      console.log(response);
      if (response.status == 401) {
        errorNoPermission(response);
      }
      return response.json();
    })
    .then(json => {

      json.operators.forEach(operator => addOperatorToFilter(operator));
      json.municipalities.forEach(municipality => {
        if (json.municipalities.length == 1) {
          // Only one area so select that one by default
          addAreaToFilter(municipality, true);
          $('.filter-area .filter-selected').html('<span>' + municipality.name + '</span>');
          reset();
          getZonesMunicipality(municipality.gm_code).then(value => addZones(value.zones));
        } else {
          addAreaToFilter(municipality);
        }
      });
    });
}

function addAreaToFilter(area, active = false) {
  var $html = ''
  $html += '<div class="radio">'
  $html += '<input type="radio" value="' + area.gm_code + '" id="' + area.gm_code + '" name="area" '
  if (active) {
    $html += 'checked'
  }
  $html += '/>'
  $html += '<label for="' + area.gm_code + '">' + area.name + '</label>'
  $html += '</div>'
  $('#filter-area').append($html);
}

function addOperatorToFilter(operator) {
  // Add operators to filter array
  selectedOperators[operator.system_id] = operator.name;
  settings.operators.add(operator.system_id)
  var $html = ''
  $html += '<div class="checkbox">'
  $html += '<input type="checkbox" checked value="' + operator.system_id + '" id="' + operator.system_id + '" name="operator"/>'
  $html += '<label for="' + operator.system_id + '">' + operator.name + '</label>'
  $html += '</div>'
  $('#filter-operator').append($html)
  $('.filter-operator').find('.filter-selected').html('');
  var newSelected = '';
  for (var property in selectedOperators) {
    if (selectedOperators.hasOwnProperty(property)) {
      newSelected += '<span>' + selectedOperators[property] + '</span>';
    }
  }
  if (newSelected == '') {
    newSelected = '<span>Selecteer een aanbieder</span>';
  }
  $('.filter-operator').find('.filter-selected').html(newSelected);
}

function getTrips() {
  // Prevent parsing trips when active_zones is null
  if ( settings.active_zones.size == 0) {
    return null;
  }
  return fetch('https://api.deelfietsdashboard.nl/dashboard-api/trips?start_time=' + settings.timestampStart
    + '&end_time=' + settings.timestampEnd + getFilterUrl(), getHeaders())
    .then(function (response) {
      console.log(response);
      if (response.status == 401) {
        errorNoPermission(response);
      }
      return response.json();
    });
}

function getRentals() {
  // Prevent parsing trips when active_zones is null
  if ( settings.active_zones.size == 0) {
    return null;
  }
  return fetch('https://api.deelfietsdashboard.nl/dashboard-api/rentals?start_time=' + settings.timestampStart
    + '&end_time=' + settings.timestampEnd + getFilterUrl(), getHeaders())
    .then(function (response) {
      console.log(response);
      if (response.status == 401) {
        errorNoPermission(response);
      }
      return response.json();
    });
}

function plotTripLines(json) {
 

  json["trips"].forEach(trip => {
    var style_icon = getMarkerTimeNotUsed(null, 0, "#FFFFFF");
    var style_end = getMarkerTimeNotUsed(null, 0, "#000000");
    style_icon.fillColor = "#21bf73";
    style_end.fillColor = "#fd5e53";
    style_icon.color = getOperatorColor(trip["system_id"]);
    style_end.color = getOperatorColor(trip["system_id"]);
    style_icon.radius = 6;
    style_end.radius = 6;

    var difference = null;
    var startTime = new Date(trip["start_time"]);
    var endTime = new Date(trip["end_time"]);
    var pt = Math.round((endTime - startTime) / 60000);

    var mtekst = "Duur: " + pt + " minuten.<br>Vertrek: " + startTime + "<br>Aankomst " + endTime + "<br> bike_id: " + trip["bike_id"];
    var mstyle = style_icon;

    plotMarker(trip["bike_id"], trip["end_location"]["latitude"], trip["end_location"]["longitude"], mtekst, style_end);
    plotMarker(trip["bike_id"], trip["start_location"]["latitude"], trip["start_location"]["longitude"], mtekst, style_icon);

  });
}

function plotRentals(data) {
  var start_icon = getMarkerTimeNotUsed(null, 0, "#21bf73");
  
  start_icon.radius = 6;
  data["start_rentals"].forEach(startOfTrip => {
    start_icon.color = getOperatorColor(startOfTrip["system_id"]);
    var mtekst = "Vertrektijd: " + startOfTrip["departure_time"] + "<br> bike_id: " + startOfTrip["bike_id"];
    plotMarker(startOfTrip["bike_id"], startOfTrip["location"]["latitude"], startOfTrip["location"]["longitude"], mtekst, start_icon);
  });

  var end_icon = getMarkerTimeNotUsed(null, 0, "#fd5e53");
  end_icon.fillColor = "#fd5e53";
  end_icon.radius = 4;
  end_icon.weight = 0.5;
  data["end_rentals"].forEach(endOfTrip => {
    end_icon.color = getOperatorColor(endOfTrip["system_id"]);
    var mtekst = "Aankomsttijd " + endOfTrip["arrival_time"] + "<br> bike_id: " + endOfTrip["bike_id"];
    plotMarker(endOfTrip["bike_id"], endOfTrip["location"]["latitude"], endOfTrip["location"]["longitude"]  + 0.00005, mtekst, end_icon);

  });
}

//
function downloadReport(start_date, end_date, gm_code, active_operators) {
  filter = "";
  if (active_operators.length > 0) {
    filter += '&operators=' + Array.from(active_operators).join(",");
  }
  url = 'https://api.deelfietsdashboard.nl/dashboard-api/stats/generate_report?start_time=' + start_date + '&end_time=' + end_date + '&gm_code=' + gm_code + filter;
  return fetch(url, getHeaders())
    .then(response => {
      if (response.status == 401) {
        errorNoPermission(response);
      }
      return response.blob();
    })
    .then(blob => {
      const url = window.URL.createObjectURL(blob);
      const a = document.createElement('a');
      a.style.display = 'none';
      a.href = url;
      a.download = "rapportage_" + start_date + "_" + end_date + "_" + gm_code + ".xlsx";
      // the filename you want
      document.body.appendChild(a);
      a.click();
      window.URL.revokeObjectURL(url);
    })
    .catch(() => alert('Downloaden mislukt.'));
}

function downloadRawData(start_date, end_date) {
  url = 'https://api.deelfietsdashboard.nl/dashboard-api/raw_data?start_time=' + start_date + '&end_time=' + end_date;
  document.getElementById("download_button_raw_data").style.display = "none";
  document.getElementById("loader_raw_data").style.display = "block";
  return fetch(url, getHeaders())
    .then(response => {
      if (response.status == 401) {
        errorNoPermission(response);
      }
      return response.blob();
    })
    .then(blob => {
      document.getElementById("download_button_raw_data").style.display = "block";
      document.getElementById("loader_raw_data").style.display = "none";
      const url = window.URL.createObjectURL(blob);
      const a = document.createElement('a');
      a.style.display = 'none';
      a.href = url;
      a.download = "export_deelfietsdashboard_" + start_date + "_" + end_date + ".zip";
      // the filename you want
      document.body.appendChild(a);
      a.click();
      window.URL.revokeObjectURL(url);
    })
    .catch((err) => {
      document.getElementById("loader_raw_data").style.display = "none";
      document.getElementById("download_button_raw_data").style.display = "block";
      alert('Downloaden mislukt.' + err);
      
    });
}


