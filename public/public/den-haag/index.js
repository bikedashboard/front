const scooter_operators = ["check", "gosharing", "felyx"];

var map = L.map('map', {
  zoomDelta: 0.25,
  zoomSnap: 0
}).setView([52.10, 4.32], 13.8);

L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/streets-v11/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoic3ZlbjRhbGwiLCJhIjoiY2pndHI1Zjk5MTRleTJxczM1cXp0dDFmYyJ9.q2gfHAYo0g84ltS2pzWJ-Q', {
  attribution: '<a href="https://www.mapbox.com/about/maps/">Mapbox</a>; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>;<strong><a href="https://gitlab.com/bikedashboard/dashboard/wikis/home">Deelfiets Dashboard</a></strong> ',
  maxZoom: 18
}).addTo(map);


async function fetchZones() {
  const response = await fetch('https://api.deelfietsdashboard.nl/dashboard-api/public/zones');
  const zones = await response.json();  
  return zones;
}

function perc2color(perc) {
  var perc = 100 - perc;
	var r, g, b = 0;
	if(perc < 50) {
		r = 255;
		g = Math.round(5.1 * perc);
	}
	else {
		g = 255;
		r = Math.round(510 - 5.10 * perc);
	}
	var h = r * 0x10000 + g * 0x100 + b * 0x1;
	return '#' + ('000000' + h.toString(16)).slice(-6);
}

function calculateGradient(number_of_vehicles, capacity) {
  var ratio = Math.min(100.0, number_of_vehicles / capacity * 100);
  return perc2color(ratio);
}

function getNumberOfScooters(operators) {
  var count = 0;
  operators.forEach( operator => {
    if (scooter_operators.includes(operator["system_id"]) ) {
      count += operator["number_of_vehicles"];
    }
  });
  return count;
}

function highlightFeature(e) {
  var layer = e.target;
  info.update(layer.feature.properties);

  layer.setStyle({
      weight: 3,
      color: '#555',
      dashArray: '',
      fillOpacity: 0.9
  });

  if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
      layer.bringToFront();
  }
}

function getScooterData(operators) {
  var scooter_content = "";
  operators.forEach( operator => {
    if (scooter_operators.includes(operator["system_id"]) ) {
      scooter_content += "<li>" + operator["system_id"] + ": " +  operator["number_of_vehicles"] + "</li>";
    }
  });
  if (scooter_content != "") {
    return "<ul>" + scooter_content + "</ul>"
  }
}

function getOtherData(operators) {
  var other_content = "";
  operators.forEach( operator => {
    if (!scooter_operators.includes(operator["system_id"]) ) {
      other_content += "<li>" + operator["system_id"] + ": " +  operator["number_of_vehicles"] + "</li>";
    }
  });
  if (other_content != "") {
    return "<ul>" + other_content + "</ul>"
  }
}


fetchZones().then(zones => {
  zones.forEach(zone => {
    var geojson;
    number_of_vehicles = getNumberOfScooters(zone["operators"]);
    var style = {fillColor: "#000066", color: "#000066", fillOpacity: 0.3, weight: 1};
    if ("capacity" in zone) {
      color = calculateGradient(number_of_vehicles, zone["capacity"]);
      style = {fillColor: color, color: color, fillOpacity: 0.7, weight: 1};
    }

    function resetHighlight(e) {
      geojson.resetStyle(e.target);
      info.update();
    }

    function onEachFeature(feature, layer) {
      layer.on({
          mouseover: highlightFeature,
          mouseout: resetHighlight
      });
    }
    console.log(getScooterData(zone["operators"]));
    data = {} 
    data["type"] = "Feature";
    data["geometry"] = zone["geojson"];
    data["properties"] = {
      "name": zone["name"], 
      "capacity": zone["capacity"], 
      "number_of_vehicles": number_of_vehicles,
      "scooter_data": getScooterData(zone["operators"]), 
      "other_data": getOtherData(zone["operators"])
    };
  

    geojson = L.geoJSON(data, {style: style, onEachFeature: onEachFeature}).addTo(map);
  });
});

var info = L.control();

info.onAdd = function (map) {
    this._div = L.DomUtil.create('div', 'info'); // create a div with a class "info"
    this.update();
    return this._div;
};

function move(ratio) {
  var elem = document.getElementById("myBar");
  var width = ratio * 100;
  var id = setInterval(frame, 10);
  var width = Math.min(100.0, ratio * 100);
  function frame() {
    elem.style.width = width + "%";
    elem.style.backgroundColor = perc2color(width);
  }
} 

// method that we will use to update the control based on feature properties passed
info.update = function (props) {
    console.log(props);
    

    this._div.innerHTML = '<h4>Parkeerzones Den Haag</h4>' +  (props ?
      '<b>' + props.name + '</b><br />' +
      'Bezetting: ' + props.number_of_vehicles + (props.capacity ? ' / ' + props.capacity : '') +
      (props.scooter_data ? "<h5>Scooter aanbieders:</h5>" + props.scooter_data : "") +
      (props.other_data ? "<h5>Andere aanbieders (tellen niet mee voor capaciteit):</h5>" + props.other_data : "" ) + 
      (props.capacity ? `<div id="myProgress">
          <div id="myBar">` +  Math.round((props.number_of_vehicles / props.capacity) * 100.0) + `%</div>
          </div>` : "")
      : 'Hover over een parkeerzone om meer informatie te zien.');
      if (props && props.capacity) {
        move(props.number_of_vehicles / props.capacity);
      }
};

info.addTo(map);
