var area2 = '',
  zone = '';
  
var municipality = null;
var activeGeoZones = {};
var initalized = false;



$(document).ready(function () {
    
    update();
    loadFilterValuesForUser().then(res => {
      $('#filter-area input').change(function () {
        municipality = $(this).val();
        var selectedLabel = $(this).siblings('label').html();
        if (selectedLabel) {
          $('.filter-area .filter-selected').html('<span>' + selectedLabel + '</span>');
        } else {
          $('.filter-area .filter-selected').html('<span>Selecteer een gebied</span>');
        }
        update();

      });
   
  });
  $('.filter-name').on('click', function (e) {
    e.preventDefault();
    $('.filter').removeClass('visible');
    $(this).parent().addClass('visible');
  });

  $(document).mouseup(function (e) {
    var container = $(".filter");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0) {
      container.removeClass('visible');
    }
  });
});

function update() {
  getDAFStats();
}

function getDAFStats() {
  getDAFOverviewStats().then(result => plotDAFOverviewTable(result));
  getDAFDepotStats().then(result => plotDAFDepotTable(result));
  getDAFCheckX().then(result => plotGraphCheckX(result));

}

function addAreaToFilter(area, active = false) {
  var $html = ''
  $html += '<div class="radio">'
  $html += '<input type="radio" value="' + area.gm_code + '" id="' + area.gm_code + '" name="area" '
  if (active) {
    $html += 'checked'
  }
  $html += '/>'
  $html += '<label for="' + area.gm_code + '">' + area.name + '</label>'
  $html += '</div>'
  console.log($html);
  $('#filter-area').append($html);
}

function loadFilterValuesForUser() {
  url = 'https://api.deelfietsdashboard.nl/dashboard-api/menu/acl';
  return fetch(url, getHeaders())
    .then(function (response) {
      console.log(response);
      if (response.status == 401) {
        errorNoPermission(response);
      }
      return response.json();
    })
    .then(json => {
      json.municipalities.forEach(municipality => {
        if (json.municipalities.length == 1) {
          // // Only one area so select that one by default
          addAreaToFilter(municipality, true);
          $('.filter-area .filter-selected').html('<span>' + municipality.name + '</span>');
          // update();
        } else {
          addAreaToFilter(municipality);
        }
      });
    });
}



function getDAFOverviewStats() {
  url = 'https://api.deelfietsdashboard.nl/daf-api/stats/bikes/overview?gm_code=GM0518';
  return fetch(url, getHeaders())
    .then(function (response) {
      console.log(response);
      if (response.status == 401) {
        errorNoPermission(response);
      }
      return response.json();
    });
}

function getDAFCheckX() {
  url = 'https://api.deelfietsdashboard.nl/daf-api/stats/events/checkx?gm_code=' + municipality;
  return fetch(url, getHeaders())
    .then(function (response) {
      console.log(response);
      if (response.status == 401) {
        errorNoPermission(response);
      }
      return response.json();
    });
}

function plotDAFOverviewTable(stats) {
  $("#daf_overview_stats_table tbody tr").remove();
  var table = document.getElementById('daf_overview_stats_table').getElementsByTagName('tbody')[0];
  summary_row = {}
  summary_row.brand = "Totaal:"
  summary_row.number_of_bikes_registered_in_daf = stats.reduce(
    (accumulator, currentValue) => accumulator + currentValue.number_of_bikes_registered_in_daf, 0
  );
  addRowToDAFOverviewTable(summary_row);
  
  stats.forEach(row => {
    console.log(row);
    addRowToDAFOverviewTable(row);
  })
}


function addRowToDAFOverviewTable(data) {
  var table = document.getElementById('daf_overview_stats_table').getElementsByTagName('tbody')[0];
  var row = table.insertRow(-1);
  var cell1 = row.insertCell(0);
  var cell2 = row.insertCell(1);
  cell1.innerHTML = data.brand.replace("<", "").replace(">", "");
  cell1.width = 200;
  cell2.innerHTML = data.number_of_bikes_registered_in_daf;
}

function getDAFDepotStats() {
  url = 'https://api.deelfietsdashboard.nl/daf-api/stats/events/overview?gm_code=' + municipality;
  return fetch(url, getHeaders())
    .then(function (response) {
      console.log(response);
      if (response.status == 401) {
        errorNoPermission(response);
      }
      return response.json();
    });
}

function plotDAFDepotTable(stats) {
  $("#depot_stats_table tbody tr").remove();
  var table = document.getElementById('depot_stats_table').getElementsByTagName('tbody')[0];
  summary_row = {}
  summary_row.brand = "Total:"
  summary_row.number_of_bikes_registered_in_daf = stats.reduce(
    (accumulator, currentValue) => accumulator + currentValue.number_of_bikes_registered_in_daf, 0
  );
  console.log(stats);
  summary_row.current_average_parking_duration = stats.reduce(
    (accumulator, currentValue) => accumulator + (currentValue.number_of_bikes_registered_in_daf * currentValue.current_average_parking_duration), 0
  ) / summary_row.number_of_bikes_registered_in_daf;
  addRowToDAFDepotTable(summary_row);
  stats.forEach(row => {
    addRowToDAFDepotTable(row);
  })
}

function addRowToDAFDepotTable(data) {
  var table = document.getElementById('depot_stats_table').getElementsByTagName('tbody')[0];
  var row = table.insertRow(-1);
  var cell1 = row.insertCell(0);
  var cell2 = row.insertCell(1);
  var cell3 = row.insertCell(2);
  cell1.innerHTML = data.brand.replace("<", "").replace(">", "");
  cell1.width = 200;
  cell2.innerHTML = data.number_of_bikes_registered_in_daf;
  cell3.innerHTML = Math.round( data.current_average_parking_duration * 10) / 10;
}


var ctx = document.getElementById('canvas').getContext('2d');
var availableBikesGraph;
var myChart = null;

function plotGraphCheckX(data) {
  graph_data = modifyDataGraphCheckX(data);
  if (myChart != null) {
    myChart.destroy();
  }

  myChart  = new Chart(ctx, {
    type: 'bar',
    data: graph_data,
    options: {
      barValueSpacing: 5,
      scales: {
        yAxes: [{
          ticks: {
            min: 0,
          }
        }]
      }
    }
  });
}

function modifyDataGraphCheckX(raw_data) {
  dates = raw_data.map(item => item.date);
  check_ins = raw_data.map(item => item.number_of_check_ins);
  check_outs = raw_data.map(item => item.number_of_check_outs);
  var data = {
    labels: dates,
    datasets: [{
      label: "Binnengebrachte fietsen",
      backgroundColor: "red",
      data: check_ins
    },  {
      label: "Opgehaalde fietsen",
      backgroundColor: "green",
      data: check_outs
    }]
  };
  return data;
}



