## FusionAuth Javascript Client ![semver 2.0.0 compliant](http://img.shields.io/badge/semver-2.0.0-brightgreen.svg?style=flat-square)
If you're integrating FusionAuth with a Javascript application, this library will speed up your development time.

For additional information and documentation on FusionAuth refer to [https://fusionauth.io](https://fusionauth.io).

Refer to the FusionAuth API documentation to for request and response formats. 
* https://fusionauth.io/docs/v1/tech/apis/
* https://fusionauth.io/docs/v1/tech/client-libraries/javascript

### Deployment

#### 1. Build new Docker image

- Go to https://gitlab.com/bikedashboard/front/container_registry
- docker login registry.gitlab.com
- [New version tag number is i.e. 1.17.2]
- docker build -t registry.gitlab.com/bikedashboard/front:1.17.2 .
- docker push registry.gitlab.com/bikedashboard/front:1.17.2

#### 2. Deploy new Docker image to Kubernetes

Below the instructions for first time Ubuntu users.

- Go to https://cloud.digitalocean.com/kubernetes/clusters?i=285f00
- Go to https://cloud.digitalocean.com/kubernetes/clusters/2c9d4c5d-a535-49ff-9d90-61a0ae272d52?i=285f00
- Click "Connecting to Kubernetes"
- sudo snap install doctl
- doctl auth init
- Go to https://cloud.digitalocean.com/account/api/tokens?i=285f00 to create a new token
- Copy token
- Paste token in terminal
- `doctl kubernetes cluster kubeconfig save 2c9d4c5d-a535-49ff-9d90-61a0ae272d52`
- `sudo snap install kubectl`
- `kubectl get deployments`
- `kubectl edit deployment dashboard-front`
- Update version number.
- Save (`:wq`)
- `kubectl get pods` (see that new one is deploying)

🎉 Deployed!
